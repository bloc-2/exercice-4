package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

/**
 * Interface définissant le comportement élémentaire d'un DAO destinée à la gestion de badges digitaux
 * PAR ACCES DIRECT, donc nécessitant la prise en compte de métadonnées
 */
public interface DirectAccessBadgeWalletDAO extends BadgeWalletDAO {

    void addBadge(DigitalBadge badge) throws IOException;

    /**
     * Permet de récupérer les metadonnées du Wallet
     *
     * @return List<DigitalBadgeMetadata>
     * @throws IOException
     */
    List<DigitalBadgeMetadata> getWalletMetadata() throws IOException;


    /**
     * Permet de récupérer un badge du Wallet à partir de ses métadonnées
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadgeMetadata meta) throws IOException;

    void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException;
}


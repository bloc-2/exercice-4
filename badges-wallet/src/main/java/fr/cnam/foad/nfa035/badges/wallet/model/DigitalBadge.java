package fr.cnam.foad.nfa035.badges.wallet.model;

import java.io.File;
import java.util.Date;
import java.util.Objects;

/**
 * POJO model représentant le Badge Digital
 */
public class DigitalBadge implements Comparable {

    private DigitalBadgeMetadata metadata;
    private File badge;
    private Date begin;
    private Date end;
    private String serial;

    /**
     * Constructeur
     *
     * @param metadata
     * @param badge
     */
    public DigitalBadge(DigitalBadgeMetadata metadata, File badge) {
        this.metadata = metadata;
        this.badge = badge;
    }

    public DigitalBadge(String s, Date begin, Date end, DigitalBadgeMetadata digitalBadgeMetadata, Object o) {
    }

    /**
     * Getter des métadonnées du badge
     * @return les métadonnées DigitalBadgeMetadata
     */
    public DigitalBadgeMetadata getMetadata() {
        return metadata;
    }

    /**
     * Setter des métadonnées du badge
     * @param metadata
     */
    public void setMetadata(DigitalBadgeMetadata metadata) {
        this.metadata = metadata;
    }

    /**
     * Getter du badge (l'image)
     * @return le badge (File)
     */
    public File getBadge() {
        return badge;
    }

    /**
     * Getter de begin
     * @return la date (Date)
     */
    public Date getBegin() {return begin;}


    /**
     * Getter de End
     * @return la date (end)
     */
    public Date getEnd() {return end;}

    /**
     * Getter de serial
     * @return le string (serial)
     */
    public String getSerial() {return serial;}

    /**
     * Setter de begin
     * @param metadata
     */
    public void setBegin(DigitalBadgeMetadata metadata) {this.begin = begin;}

    /**
     * Setter de End
     * @param metadata
     */
    public void setEnd(DigitalBadgeMetadata metadata) {this.end = end;}

    /**
     * Setter de serial
     * @param metadata
     */
    public void setSerial(DigitalBadgeMetadata metadata) {this.serial = serial;}




    /**
     * Setter du badge (Fichier image)
     * @param badge
     */
    public void setBadge(File badge) {
        this.badge = badge;
    }

    /**
     * {@inheritDoc}
     * @param o
     * @return boolean
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DigitalBadge that = (DigitalBadge) o;
        return metadata.equals(that.metadata) && badge.equals(that.badge) && serial.equals(that.serial) && begin.equals(that.begin);
    }

    /**
     * {@inheritDoc}
     *
     * @return int
     */
    @Override
    public int hashCode() {
        return Objects.hash(metadata, badge, serial, begin);
    }

    /**
     * {@inheritDoc}
     * @return
     */
    @Override
    public String toString() {
        return "DigitalBadge{" +
                "metadata=" + metadata +
                ", badge=" + badge +
                ",serial=" + serial +
                "begin=" + begin +
                '}';
    }

    @Override
    public int compareTo(Object o) {
        DigitalBadgeMetadata that = (DigitalBadgeMetadata) o;
        return that.compareTo(o);
    }
}
